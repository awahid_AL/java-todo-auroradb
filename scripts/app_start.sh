#!/usr/bin/env bash

set -xeu

chmod +x /app/scripts/sql_schema.sh

chmod +x /app/scripts/entrypoint.sh

# Configure Database connection
/app/scripts/entrypoint.sh /app/scripts/sql_schema.sh

# Deploy app
nohup /app/scripts/entrypoint.sh java -jar /app/java-application/target/demo-0.0.1-SNAPSHOT.jar 1>/tmp/server.log 2>/tmp/server.err &
