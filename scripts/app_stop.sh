#!/usr/bin/env bash

set -xe

for pid in $(ps -ef | grep -v grep | grep java | awk '{ print $2 }'); do
  kill -9 "${pid}"
done

echo "[WebApp] App stopped"
